class Food {
    constructor(url) {
        this.url = url;
    }

    /***
     * this method gets form and returns all content,
     * it first of all get the form, check if empty then all,
     * after all it returns every aside button (submit)
     * @param  none
     * **/
    getFormData () {
        // getting form ready
        const formElements = document.querySelector(".make-new-input").elements;

        // checking for emptiness
        if (formElements == null){
            alert("Form Not Available Please set form name!");
        }else {
            const  fort = new FormData();
            for (var i = 0; i < formElements.length; i++) {
                if (formElements[i].type != "submit" ) {

                    if (formElements[i].type == "file"){
                        for (const  file of formElements[i].files){
                            fort.append("myFiles[]",file);
                        }
                    }else {
                        fort.append(formElements[i].name, formElements[i].value);
                    }

                }
            }

            // return array.join('&');
            return fort;
        }
    }

    /***
     * * this method is for sending data into controller
     *   this gets form content firstly then it send
     *   via ajax to the controller for proccess
     *   this method uses the URL path that comes from the class constructor at initiation point
     *   @param null
     * ***/
    SendToController () {

        // send to controller via ajax, return message as response

        const xhr = new XMLHttpRequest();
        xhr.open("post",this.url,true);
        xhr.onreadystatechange = function () {
            console.log(xhr.responseText +" NNN ");
            if (xhr.readyState == 4 && xhr.status  == 200) {
                if (xhr.responseText) {
                    console.log(xhr.responseText + " ggg ");

                    alert('Done!');
                }
            }
        }
        xhr.send(this.getFormData());
    }


     fetchSchoolByID (id) {
      const select = document.querySelector('.modal-loadContent');
       const url = `../../../../Main/includes/Curriculum.php?key=1234567opiuyt&method=select&id=${id}`;
          const xhr = new XMLHttpRequest();
          xhr.open('GET', url, true);
          xhr.onreadystatechange = function () {
            if (xhr.readyState == 4 && xhr.status == 200) {

                let dbContent = JSON.parse(xhr.responseText);
                let content = `
                        <form class="new-added-form">
                            <div class="row">
                                <div class=" col-lg-12  form-group">
                                    <label class=" col-lg-5 "> Class *</label> <label class=" col-lg-5 "> ${dbContent.class} *</label>
                                </div>
                                <div class=" col-lg-12 form-group">
                                    <label class=" col-lg-6 "> Session *</label> <label class=" col-lg-6 "> ${dbContent.session} *</label>
                                </div>
                                <div class=" col-lg-12  form-group">
                                    <label class=" col-lg-offset-1 col-lg-10">Message </label>
                                        ${dbContent.message}
                                </div>

                                <div class="col-md-6 form-group"></div>
                                <div class="col-lg-12 form-group mg-t-8">
                                    <button type="submit" class="btn-fill-lg btn-gradient-yellow btn-hover-bluedark">Add</button>
                                </div>
                            </div>
                        </form>
                            `;
                select.innerHTML ='';
                select.insertAdjacentHTML('beforeend', content);
            }
          }
         xhr.send();
    }

    doClick (){
        // document.querySelector(".dope-card").addEventListener('click',eve => {
        //     if ( eve.target.matches('.loadSchool-inn, .loadSchool-inn *')){
        //         let targets =eve.target.dataset.tag;
        //         this.fetchSchoolByID(targets);
        //     }
        // });


        document.querySelector(".add-to-btn").addEventListener('click',eve => {
            eve.preventDefault();
            this.SendToController ();
            alert("Hey...!");
        });


    }


}

const food = new Food("./includes/Sell.php?key=1234567opiuyt&method=insert");
food.doClick();

