<?php include '../includes/Food.php';


?>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
	<meta charset="utf-8">
	<title>Foodies</title>
	<meta name="description" content="">
	<!-- Mobile Specific Meta -->
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<!-- <link rel="shortcut icon" href="img/favicon.png"> -->

	<link href='http://fonts.googleapis.com/css?family=Lato:300,400,700' rel='stylesheet'>

	<!-- Syntax Highlighter -->
	<link rel="stylesheet" type="text/css" href="syntax-highlighter/styles/shCore.css" media="all">
	<link rel="stylesheet" type="text/css" href="syntax-highlighter/styles/shThemeDefault.css" media="all">

	<!-- Font Awesome CSS-->
	<link rel="stylesheet" href="css/font-awesome.min.css">
	<!-- Normalize/Reset CSS-->
	<link rel="stylesheet" href="css/normalize.min.css">
	<!-- Main CSS-->
	<link rel="stylesheet" href="css/main.css">
	<!-- Main CSS-->
	<link rel="stylesheet" href="css/bootstrap.min.css">

</head>

<body id="welcome">

	<aside class="left-sidebar">
		<div class="logo">
			<a href="#welcome">
				<h1>Foodies Admin </h1>
			</a>
		</div>
		<nav class="left-nav">
			<ul id="nav">
				<li class="current"><a href="#welcome">Welcome</a></li>
				<li><a href="#tmpl-structure">Orders</a></li>
				<li><a href="#Food-History">Food History</a></li>
				<li><a href="logout.php">Logout</a></li>

			</ul>
		</nav>
	</aside>

	<div id="main-wrapper">
		<div class="main-content">
			<section id="welcome">
				<div class="content-header">
					<h1>Foodies Admin</h1>
				</div>
				<div class="welcome">
					<h2 class="twenty">Welcome </h2>

				</div>

				<div class="features">
					<h1>Food Upload</h1>
					<form method="post" enctype="multipart/form-data" class="make-new-input">
						<div class="form-group">
							<input class="form-control" type="text" name="title"  placeholder="Title" required=""/>
						</div>
						<div class="form-group">
							<input type="text" class="form-control"  name="category" placeholder="Category"/>
						</div>
						<div class="form-group">
							<input type="text" class="form-control" name="price" placeholder="Price"/>
						</div>
						<div class="form-group">
							<input  type="text" class="form-control" name="description" placeholder="Description"/>
						</div>
                        <div class="form-group">
							<input  type="file" class="form-control"  placeholder="Description"/>
						</div>
						<div class="form-group">
						<button type="submit" class="btn btn-default add-to-btn">Send</button>
						</div>

					</form>
				</div>

			</section>




			<section id="tmpl-structure">
				<h2 class="title">Customers Order</h2>
				<p class="fifteen">All information within the main content area.</p>

                                    <table class="table table-responsive">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>Address</th>
                                                <th>Price</th>
                                                <th>Quantity</th>

                                            </tr>
                                        </thead>
                                        <tbody>
                                        <?php
                                        $sells = $sell->getAllScheduleBySql("select * from sell order by id desc");
                                        $count = 0;
                                        foreach ($sells as $eachSells => $value ){
                                        $count ++;

                                        ?>
										<tr>
                                                <th scope="row"><?php echo $count;?></th>
                                                <td><?php echo $sells[$eachSells]["address"];?></td>
                                                <td><?php echo $sells[$eachSells]["price"];?></td>
                                                <td><?php echo $sells[$eachSells]["quantity"];?></td>



                                            </tr>
										<?php
                      }
                      ?>
                                        </tbody>
                                    </table>


					<footer class="footer">

					</footer>
				</pre>
			</section>	<section id="Food-History">
				<h2 class="title">Food History</h2>
				<p class="fifteen">All information within the main content area.</p>

                                    <table class="table table-responsive">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>Name</th>
                                                <th>Category</th>
                                                <th>Price</th>
                                                <th>Description</th>
                                                <th>Look Out</th>

                                            </tr>
                                        </thead>
                                        <tbody>
                                        <?php
                                        $foods = $food->getAllFood();
                                        $count = 0;
                                        foreach ($foods as $eachFood => $value ){
                                         $count ++;

                        ?>
										<tr>
                                                <th scope="row"><?php  echo $count;?></th>
                                                <td><?php echo $foods[$eachFood]["title"];?></td>
                                                <td><?php echo $foods[$eachFood]["category"];?></td>
                                                <td><?php echo $foods[$eachFood]["price"];?></td>
                                                <td><?php echo $foods[$eachFood]["description"];?></td>
                                                <td><?php echo "<img style='width: 100px;' src='../models/forAllImage /".$foods[$eachFood]["image"]."'"."alt='Loading...'/>";?></td>
                                            </tr>
										<?php
                                 }
                      ?>
                                        </tbody>
                                    </table>


					<footer class="footer">

					</footer>
				</pre>
			</section>

  </div>
</div>


		<!-- Essential JavaScript Libraries
			==============================================-->
			<script type="text/javascript" src="js/jquery-1.11.0.min.js"></script>
			<script type="text/javascript" src="js/jquery.nav.js"></script>
			<script type="text/javascript" src="syntax-highlighter/scripts/shCore.js"></script> 
			<script type="text/javascript" src="syntax-highlighter/scripts/shBrushXml.js"></script> 
			<script type="text/javascript" src="syntax-highlighter/scripts/shBrushCss.js"></script> 
			<script type="text/javascript" src="syntax-highlighter/scripts/shBrushJScript.js"></script> 
			<script type="text/javascript" src="syntax-highlighter/scripts/shBrushPhp.js"></script> 
			<script type="text/javascript">
				SyntaxHighlighter.all()
			</script>
			<script type="text/javascript" src="js/custom.js"></script>
            <script type="text/javascript" src="js/Food.js"></script>

		</body>
		</html>
